site = IO.readlines("input/site.conf", chomp: true)
site_domain = site[0]
site_name = site[1]
theme = site[2]
themes_fold = "themes/"
theme_dir = themes_fold + theme + "/"
protocol = "https://"

file = Marshal.load(Marshal.dump(ARGV[0]))

if file.include? "html"
	text = File.read(file)
	template = File.read(theme_dir + "template.html")

        product = template

	puts file

	lines = text.lines
	rest_of_content = lines[1..-1]
	raw_content = rest_of_content.join()
	title = lines[0]
	
	content = raw_content

	if product.include? "{SITE_NAME}"
	while product.include? "{SITE_NAME}"
		product["{SITE_NAME}"]= site_name
	end
	end 

	if product.include? "{SITE_DOMAIN}"
	while product.include? "{SITE_DOMAIN}"
		product["{SITE_DOMAIN}"]= site_domain
	end
	end

	if product.include? "{PAGE_TITLE}"
	while product.include? "{PAGE_TITLE}"
		product["{PAGE_TITLE}"]= title
	end
	end 
	if product.include? "{PAGE_CONTENT}"
	while product.include? "{PAGE_CONTENT}"
		product["{PAGE_CONTENT}"]= content
	end
	end 

	handle = file
	handle["input"]= "output"
	IO.write(handle, product)
end
