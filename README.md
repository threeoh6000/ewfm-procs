# ewfm processors
This repository contains some processors.

## Contributing
The processors in this repository must be open-source and must not span over 2MB in space.

**All** themes in this repository will be licensed under the GNU GPL.

Developers are allowed to add an extra `AUTHORS` file to denote who worked on said processors.
